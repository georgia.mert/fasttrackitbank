package org.fasttrackit;
//Create user (user/ password/ name/surname/email/ phone) (/)
//Login user
//Display account numbers (IBAN)
//Display account balance
//Create new account
//Exchange rates
//Transfer between accounts

import java.io.InputStream;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Scanner;

import static org.fasttrackit.AccountFactory.createAccount;
import static org.fasttrackit.View.*;

public class Main {

    public static final int MAX_LOGIN_RETRY = 3;
    private static boolean authenticated = false;
    private static User user = null;
    private static String userOption = null;

    public static void main(String[] args) {
        System.out.println("     - = FastTrack Bank = -");
        System.out.println();
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);

        showWelcomingScreen();

        userOption = keyboard.nextLine();
        while (!userOption.equals(EXIT)) {
            runBankingApp(keyboard);
        }
            System.out.println("GoodBye.");
    }

    private static void runBankingApp(Scanner keyboard) {
        if (!authenticated && userOption.equals("1")) {
            askUserDetails(keyboard);
            showNavigationMenu();
            return;
        }
        if (!authenticated && userOption.equals("2")) {
            user = askUserToLogin(keyboard);
        }
        if (!authenticated || user == null) {
            showContactSupport();
            return;
        }
        showNavigationMenu();
        userOption = keyboard.nextLine();
        if (userOption.equals(VIEW_ACCOUNT_DETAILS)) {
            String iban = user.getAccount().getIban();
            String currency = user.getAccount().getCurrency();

            showAccountDetails(iban, currency);
            List<Account> accounts = user.getAccounts();
            for (Account account : accounts) {
                showAccountDetails(account.getIban(), account.getCurrency());
                
            }
        }
        if (userOption.equals(CHECK_ACCOUNT_BALANCE)) {
            System.out.println("Account balance:" + user.getAccount().getBalance());
        }
        if (userOption.equals(CREATE_NEW_ACCOUNT)) {
            showSupportedCurrency();
            Account account = createAccount(keyboard.nextLine());
            if (account != null) {
                user.registerAccount(account);
                System.out.println("New Account with - IBAN:" + account.getIban() + " - Currency:" + account.getCurrency() + " has been created.");
            }
        }
    }

    private static User askUserToLogin(Scanner keyboard) {
        User bankUser = fetchUser();
        //If bank user is authenticated with credentials...
        // display user and phone number

        for (int i = 0; i < MAX_LOGIN_RETRY; i++) {
            Credentials credentials = askForCredentials(keyboard);
            authenticated = bankUser.isAuthenticated(credentials);
            if (!authenticated) {
                System.out.println("Login id or password is incorrect");

            }
            if (authenticated) {
                break;
            }
        }
        if (authenticated) {
            System.out.println();
            System.out.println("Welcome back mr. " + bankUser.getName());
            System.out.println();
            return bankUser;
        }
        return null;
    }


    private static Credentials askForCredentials(Scanner keyboard) {
        System.out.print("Please enter your login id:");
        String loginId = keyboard.nextLine();
        System.out.print("Please enter your password: ");
        String password = keyboard.nextLine();
        return new Credentials(loginId, password);
    }

    private static User fetchUser() {
        String user = "georgia.mert";
        String password = "123Meca";
        String name = " Mert ";
        String surname = " Georgi ";
        String email = " georgia.mert1@gmail.com ";
        String phoneNr = "0770504870";
        User bankUser = new User(user, password, name, surname);
        bankUser.setEmail(email);
        bankUser.setPhone(phoneNr);
        return bankUser;
    }
}