package org.fasttrackit;

public class Account {
    private final String iban;
    private final String currency;
    private double balance;
    private String name;

    public Account(String iban, String currency) {
        this.iban = iban;
        this.currency = currency;
    }

    public String getIban() {
        return iban;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
