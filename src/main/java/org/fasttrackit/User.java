package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

//(user/ password/ name/surname/email/ phone)
public class User {
    private final Credentials credentials;
    private final Account defaultAccount;
    private final List <Account> accounts;
    private final String name;
    private final String surname;
    private String email;
    private String phone;

    public User(String loginID, String password, String name, String surname) {
        this.credentials = new Credentials(loginID, password);
        this.defaultAccount = new Account ( "RO55BTRLRONCRTL01100i1010",  "RON");
        this.name = name;
        this.surname = surname;
        this.email = "";
        this.phone = "";
        this.accounts = new ArrayList<>();
    }
    public void registerAccount (Account newAccount) {
        this.accounts.add(newAccount);

    }

    public boolean isAuthenticated (Credentials credentials) {
        return this.credentials.equals(credentials);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Account getAccount() {
        return defaultAccount;
    }

    public String getName() { return name; }

    public String getSurname() { return surname; }

    public String getEmail() { return email; }

    public String getPhone() { return phone; }

    public void setEmail(String email) { this.email = email; }

    public void setPhone(String phone) { this.phone = phone; }
}
