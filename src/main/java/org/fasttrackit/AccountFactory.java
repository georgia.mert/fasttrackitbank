package org.fasttrackit;

public class AccountFactory {

    static Account createAccount(String userOption) {
        Account account = null;
        switch (userOption) {
            case "1": {
                account = new Account( "RO55BTRLRONCRTL01100i1011", "RON");
                break;
            }
            case "2": {
                account = new Account( "RO55BTRLEURCRTL01100i1011", "EUR");
                break;
            }
            case "3": {
                account = new Account( "RO55BTRLUSDCRTL01100i1011", "USD");
                break;
            }
            case "4": {
                account = new Account( "RO55BTRLGBPCRTL01100i1011", "GBP");
                break;
            }
            default:
                System.out.println("sorry, unknown option");
        }
        return account;
    }
}
